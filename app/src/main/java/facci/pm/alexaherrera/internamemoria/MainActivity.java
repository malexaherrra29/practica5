package facci.pm.alexaherrera.internamemoria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText cedula, apellidos, nombres;
    TextView texto;
    Button leer, escribir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cedula = (EditText)findViewById(R.id.cedula);
        apellidos = (EditText)findViewById(R.id.apellidos);
        nombres = (EditText)findViewById(R.id.nombres);
        leer = (Button)findViewById(R.id.leer);
        escribir = (Button)findViewById(R.id.escribir);
        texto = (TextView)findViewById(R.id.texto);

        leer.setOnClickListener(this);
        escribir.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.leer:
                try {
                    OutputStreamWriter writer = new OutputStreamWriter(openFileOutput("archivo.txt",
                            Context.MODE_APPEND));
                    writer.write(cedula.getText().toString()+
                            ","+apellidos.getText().toString()+
                            ","+nombres.getText().toString());
                } catch (Exception e) {
                    Log.e("Archivo MI", "Error en el archivo de escritura");
                }
                break;
            case R.id.escribir:
                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(openFileInput(
                            "archivo.txt")));
                    String datos = reader.readLine();
                    String [] listaPersonas = datos.split(";");
                    for(int i = 0; i < listaPersonas.length; i++){
                        texto.append(listaPersonas[i].split(",")[0]+
                                ""+listaPersonas[i].split(",")[1]+
                                ""+listaPersonas[i].split(",")[2]);
                    }
                    reader.close();
                }catch (Exception e){
                    Log.e("Archivo MI", "Error en el archivo de lectura"+e.getMessage());
                }
                break;
                }
        }
    }

